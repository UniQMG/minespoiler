const numbers = ['0⃣', '1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣'];
const bomb = '💣';

module.exports = function generate(width, height, mines) {
  let field = [];

  for (var x = 0; x < width; x++) {
  	field[x] = []
  	for (var y = 0; y < height; y++) {
  		field[x][y] = 0;
  	}
  }

  for (var i = 0; i < mines; i++)
  	field[Math.floor(Math.random()*width)][Math.floor(Math.random()*height)] = 'bomb';

  let checks = [
  	[-1,-1], [ 0,-1], [ 1,-1],
  	[-1, 0],          [ 1, 0],
  	[-1, 1], [ 0, 1], [ 1, 1]
  ];

  for (var x = 0; x < width; x++) {
  	for (var y = 0; y < height; y++) {
      if (field[x][y] == 'bomb') continue;
  		let risk = checks.map(([xm, ym]) => {
  			return (field[x+xm] || [])[y+ym] == 'bomb' ? 1 : 0;
  		}).reduce((a,b) => a+b, 0);
  		field[x][y] = risk;
  	}
  }

  let minesweeper = field
  	.map(arr => arr.map(arr => arr == 'bomb' ? bomb : numbers[arr])
  	.map(e => `||${e}||`).join('​'))
  	.join('\n');

  return minesweeper;
}
