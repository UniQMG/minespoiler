const minesweeper = require('./minesweeper');
const discord = new (require('instacord2'));
const version = '1.1.1';

discord.memstore().login(require('./secret.js'));
process.on('unhandledRejection', (err) => {
  if (!err) return;
  console.error(err);
  process.exit(-1);
});
discord.load(); // Workaround for DM crashbug in instacord

let app = new (require('instacord2').Router)();

discord.client.on('ready', () => {
  console.log('ready');
  discord.client.user.setActivity('prefix: minesweeper', { type: 'WATCHING' });
  discord.mount((sub, msg, actions) => {
    if (msg.author.bot) return false;
    let mention1 = '<@!' + discord.client.user.id + '>';
    let mention2 = '<@' + discord.client.user.id + '>';
    let prefix = 'minesweeper';

    if (sub.startsWith(mention1)) return mention1;
    if (sub.startsWith(mention2)) return mention2;
    if (sub.startsWith(prefix)) return prefix;
    return false;
  }, app);
});

app.cmd('gen', (sub, msg, actions) => {
  let args = sub.trim().split(' ');
  if (args.length == 2) {
    // Only set size if given 2 parameters
    args = [10, args[1], args[2]];
  }

  let mines  = Math.max(Math.min(Number.parseInt(args[0]) || 10, 250), 1);
  let width  = Math.max(Math.min(Number.parseInt(args[1]) || 10, 20), 1);
  let height = Math.max(Math.min(Number.parseInt(args[2]) || 10, 20), 1);
  console.log(mines, width, height);

  let field = minesweeper(width, height, mines)

  let embedFields = [];
  let lines = field.split('\n');
  let linesPerField = Math.floor(1024 / (7 * width));

  if (lines.length < linesPerField) {
    embedFields.push({
      name: `Minefield`,
      value: field
    })
  } else {
    for (var i = 0; i < lines.length; i += linesPerField) {
      embedFields.push({
        name: `Rows ${i+1}-${Math.min(lines.length, i+linesPerField)}`,
        value: lines.slice(i, i+linesPerField).join('\n')
      })
    }
  }

  msg.channel.send({
    embed: {
      color: 0x62f911,
      timestamp: (new Date()).toString(),
      author: {
        name: `Generated ${width}x${height} with ${mines} mines`,
        icon_url: discord.client.user.avatarURL
      },
      footer: {
        icon_url: msg.client.user.avatarURL,
        text: "Minespoiler v" + version
      },
      fields: embedFields
    }
  });
});

app.cmd(/^help|info/, (sub, msg, actions) => {
  msg.channel.send({
    embed: {
      color: 0x62f911,
      timestamp: (new Date()).toString(),
      author: {
        name: discord.client.user.username,
        icon_url: discord.client.user.avatarURL
      },
      footer: {
        icon_url: msg.client.user.avatarURL,
        text: "Minespoiler v" + version
      },
      description: "Generates minesweeper-esque spoilered emojis\n" +
                   "Does not do anything else, including win/lose messages",
      fields: [{
        name: "Commands",
        value: "```"
        + "\nminesweeper gen <mines> <x> <y> | Generates a minefield"
        + "\nminesweeper help                | This help text."
        + "```"
      }, {
        name: "Invite me!",
        value: "[discordapp.com](https://discordapp.com/oauth2/authorize?" +
               "client_id=541106685104619560&scope=bot&permissions=2048)",
        inline: true
      }, {
        name: "Guild count",
        value: msg.client.guilds.size,
        inline: true
      }, {
        name: "Prefix",
        value: 'minesweeper',
        inline: true
      }, {
        name: "Author",
        value: '**UniQMG**​*#0522*',
        inline: true
      }, {
        name: "My website with other projects",
        value: "https://uniq.mg/",
        inline: true
      }]
    }
  });
});
