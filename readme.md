Minespoiler

A discord minesweeper bot. Not interactive, just generates arrays of emoji.

Usage:

    git clone https://gitlab.com/UniQMG/minespoiler
    npm install
    cat $YOUR_BOT_TOKEN > ./secret.js
    node
